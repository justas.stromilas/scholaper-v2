DROP TABLE scholar_progress;
CREATE TABLE scholar_progress (
	id serial PRIMARY KEY,
	progress_type     VARCHAR ( 250 ) NOT NULL,
	last_page         TEXT UNIQUE NOT NULL,
	uni_web_address   VARCHAR ( 255 )  NOT NULL,
	country           VARCHAR ( 255 )  NOT NULL,
	status            VARCHAR ( 255 )  NOT NULL,
	updated_on        timestamp(0),
    created_on        timestamp(0) default  now() NOT NULL
);

update university set scholar_link= 'https://scholar.google.com/citations?hl=en&view_op=search_authors&mauthors=' ||web_address||'&btnG=';
INSERT INTO scholar_progress(progress_type,last_page, uni_web_address,country,status) SELECT scholar_link,scholar_link, web_address,country,country FROM university;
update scholar_progress set status = FALSE ,  progress_type = 'scholar';