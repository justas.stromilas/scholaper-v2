import os
import yaml
from component.chrome_driver.chrome_driver import get_driver_directory
from pathlib import Path

# project root directory
ROOT_DIR = str(Path(os.path.dirname(os.path.abspath(__file__))).parent) + str(os.sep)

# chrome driver directory
CHROME_DRIVER_DIR = get_driver_directory()

# config.yml specifications
CONFIG_PATH = os.path.join(ROOT_DIR, 'config.yml')
with open(CONFIG_PATH, 'r') as stream:
    defined_config = yaml.load(stream, yaml.SafeLoader)
DB_NAME = defined_config['DatabaseConfig']['name']
DB_USER = defined_config['DatabaseConfig']['user']
DB_PASS = defined_config['DatabaseConfig']['password']
DB_HOST = defined_config['DatabaseConfig']['host']
DB_PORT = defined_config['DatabaseConfig']['port']
CHROME_DRIVER_VERSION = defined_config['ChromeDriver']['version']
