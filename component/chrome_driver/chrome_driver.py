from selenium import webdriver
import os, inspect
import platform


def get_driver_directory():
    plt = platform.system()
    if plt == "Windows":
        dir_path = os.path.dirname(os.path.realpath(__file__)) + os.sep + "driver" + os.sep + "chrome-driver-version-94.0.4606.61.exe"
    else:
        print("Operating system not supported yet !")
    return dir_path


def get_driver_options():
    options = webdriver.ChromeOptions()
    options.add_argument("--log-level=3")
    options.add_argument('--deny-permission-prompts')
    options.add_argument("enable-automation")
    # self.options.add_argument('--headless')
    options.add_argument("--window-size=1920,1080")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-extensions")
    options.add_argument("--dns-prefetch-disable")
    options.add_argument("--disable-gpu")
    preferences = {"profile.default_content_setting_values.geolocation": 2}
    options.add_experimental_option("prefs", preferences)
    return options


if __name__ == "__main__":
    get_driver_directory()
