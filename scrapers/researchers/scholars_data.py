import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException

from utils.presistance_tools.database.repository.scholar import Scholar
from utils.presistance_tools.database.repository.progress import Progress
from component.chrome_driver.chrome_driver import get_driver_options, get_driver_directory
from component.proxy_loader.proxy_bouncer import reload_fresh_proxy, update_multiple_expired_status


def filtered(element, word):
    try:
        element = element.split(word)[1]
        return element
    except Exception as e:
        element = 0
        return element


def check_next_page(chrome_browser):
    if chrome_browser.find_elements_by_xpath(
            "//button[@class='gs_btnPR gs_in_ib gs_btn_half gs_btn_lsb gs_btn_srt "
            "gsc_pgn_pnx']"):
        try:
            next_button = \
                chrome_browser.find_elements_by_xpath("//button[@class='gs_btnPR gs_in_ib gs_btn_half gs_btn_lsb "
                                                      "gs_btn_srt gsc_pgn_pnx']")[0]
            if next_button.get_attribute("onclick"):
                next_page_url = next_button.get_attribute("onClick")
                url_key = next_page_url[17:-1].replace("\\/", "/").encode().decode('unicode_escape')
                next_page_url = "https://scholar.google.com" + url_key
                print("Next Page Url", next_page_url)

                chrome_browser.set_page_load_timeout(30)

                return next_page_url

            else:
                print("no next page found !")
                return False

        except Exception as e:
            print(e)
            print("exception in main while loop")
            return False
    else:
        return False


def quit_brower(chrome_browser):
    try:
        chrome_browser.quit()
    except Exception as e:
        print("Browser already closed !")


def start_scholar_scraping(db_connection):
    scholar_data = Scholar(db_connection)
    progress = Progress(db_connection)
    options = get_driver_options()
    proxies = reload_fresh_proxy(db_connection)
    if len(proxies) == 0:
        print("out of proxies")
        sys.exit()

    options.add_argument('--proxy-server={}'.format(proxies[0]))
    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())
    last_progress = progress.get_scholar_progress()
    proxy_count = 0

    for last_page, web_address, country in last_progress:
        print("Moving to next uni ==> {} \ncountry ==> {} \nLast page ==> {}".format(web_address, country, last_page))
        while True:

            if proxy_count == 29:
                quit_brower(chrome_browser)
                print("Updating proxy expired list")
                update_multiple_expired_status(proxies, db_connection)
                proxies = reload_fresh_proxy(db_connection)
                proxy_count = 0
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

            try:

                chrome_browser.get(last_page)

                chrome_browser.set_page_load_timeout(50)

                if 'sorry' in chrome_browser.current_url:
                    print("Bot Protection Url detected...")
                    proxy_count = proxy_count + 1
                    quit_brower(chrome_browser)
                    options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

                elif 'Sorry' in chrome_browser.title:
                    print("bot detection url.........")
                    proxy_count = proxy_count + 1
                    chrome_browser.quit()
                    options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

                else:
                    page_text = chrome_browser.find_element_by_tag_name('body').text

                    if "Your search - {} - didn't match any user profiles.".format(web_address) in page_text:
                        print("No profiles found for the provided uni web address: {} ".format(web_address))
                        progress.update_scholar_uni_progress(web_address)
                        print("University marked as finished")
                        break

                    links = WebDriverWait(chrome_browser, 50).until(
                        EC.visibility_of_all_elements_located((By.CSS_SELECTOR, '.gs_ai_name a')))
                    name = chrome_browser.find_elements_by_xpath("//h3[@class='gs_ai_name']")
                    emails = chrome_browser.find_elements_by_xpath("//div[@class='gs_ai_eml']")
                    citations = chrome_browser.find_elements_by_xpath("//div[@class='gs_ai_cby']")
                    field_tags = chrome_browser.find_elements_by_xpath("//div[@class='gs_ai_int']")

                    for l, e, c, ft in zip(links, emails, citations, field_tags):
                        name = l.text
                        email = filtered(e.text, 'at')
                        citation = filtered(c.text, 'by')
                        scholar_id = l.get_attribute('href')
                        scholar_id = scholar_id.split('=')
                        field_tag = ft.text
                        scholar_data.add_new_scholar(scholar_id[2], name, email, citation, country, field_tag)

                    next_page = check_next_page(chrome_browser)

                    if next_page == False:
                        print("No next page found,\nUpdating progress for {}".format(web_address))
                        progress.update_scholar_uni_progress(web_address)
                        print("University marked as finished")
                        break
                    else:
                        last_page = next_page
                        print("New next page found {},\nUpdating progress for {}".format(last_page, web_address))
                        progress.update_scholar_page_progress(last_page, web_address)


            except TimeoutException as ex:
                print("Time out exception while loading page {}".format(last_page))
                print("proxy expired iteration count ==> {} \nProxy address expired: {}".format(str(proxy_count),
                                                                                                str(proxies[
                                                                                                        proxy_count])))
                proxy_count = proxy_count + 1
                quit_brower(chrome_browser)
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

            except WebDriverException as ex:
                print("Web Driver exception while loading page {}".format(last_page))
                print("Proxy expired iteration count ==> {} \nProxy address expired: {}".format(str(proxy_count),
                                                                                                str(proxies[
                                                                                                        proxy_count])))
                proxy_count = proxy_count + 1
                quit_brower(chrome_browser)
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())
