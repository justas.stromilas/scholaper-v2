from utils.presistance_tools.database.db_connection import DatabaseConnection
from bs4 import BeautifulSoup
import re
import requests

TOP_RANKING_UNI_URL = "https://www.webometrics.info/en/"


def get_countries():
    db = DatabaseConnection()
    result = db.select_query_general('select name,continent from countries;')
    db.close()
    return result


def save_uni_details(uni_name, country, uni_link, continent, rank):
    db = DatabaseConnection()
    db.query(
        "INSERT INTO university (uni_name,country,web_address,continent,rank) values ('" + uni_name + "','" + country + "','" + uni_link + "','" + continent + "'," + str(
            rank) + ");")
    db.close()


def prepare_url(continent, country):
    webometrics_url = TOP_RANKING_UNI_URL + str(continent) + "/" + country
    print("webometrics url ==> ", webometrics_url)
    return webometrics_url


def get_page_code(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'lxml')
    table = soup.find('table', {'class': 'sticky-enabled'})
    a_tags = table.find_all('a', text=True, attrs={'href': re.compile("^https|http://")})
    return a_tags


def filter_uni_domain(domain):
    if domain.endswith('/'):
        domain = domain[:-1]
    if domain.startswith('https://'):
        domain = domain[8:]
    if domain.startswith('http://'):
        domain = domain[7:]
    if domain.startswith('www'):
        domain = domain[3:]
    if domain.startswith('.'):
        domain = domain[1:]
    return domain


def extract_uni_data():
    top_ranked_limit = 35
    countries = get_countries()
    for country in countries:
        continent = country[1]
        country = country[0]
        url = prepare_url(continent, country)
        a_tags = get_page_code(url)
        uni_data = list()
        rank = 0
        for a_tag in a_tags:
            rank = rank + 1
            uni_link = filter_uni_domain(a_tag.get('href'))
            uni_name = a_tag.text
            save_uni_details(uni_name, country, uni_link, continent, rank)
            if rank == top_ranked_limit:
                break


'''
extract_uni_data()
'''
