import argparse
import sys
import multiprocessing
from utils.presistance_tools.database.db_connection import DatabaseConnection
from component.proxy_loader import proxy_bouncer
from scrapers.researchers.scholars_data import start_scholar_scraping
import threading
import time
import keyboard

app_threads_status = [False]
main_connection = DatabaseConnection()

def start_researcher_thread():
    app_thread = threading.Thread(target= start_scholar_scraping, args=(main_connection,))
    app_thread.start()
    app_thread.join()



def start_reference_thread():
    print( "\nquak from thread 2")


def start_email_thread():
    print( "\nsquak from thread 3")


def start_threads():
    if app_threads_status[0] == False:
        app_threads_status[0] = True
        start_researcher_thread()
        start_reference_thread()
        start_email_thread()
    else:
        print("Inside the main thread !")


def main_app():
    old_time = time.time()

    while True:
        if time.time() - old_time > 3600:
            print("new proxy updated")
            proxy_bouncer.start_proxy_thread(main_connection)
            old_time = time.time()

        if app_threads_status[0] == False:
            print("Starting all threads")
            start_threads()

        if keyboard.is_pressed("q"):
            break


if __name__ == "__main__":
    app_thread = threading.Thread(target=main_app)
    app_thread.start()
    app_thread.join()
    main_connection.close_db_connection()
    print("\nApplication closed ! Goodbye ")
    sys.exit()
